// Подключение слайдеров

// const { FALSE } = require("node-sass");

$(document).ready(function () {
  const sliders = document.querySelectorAll(".portfolio-section-slider__item");
  let countSlide = 5;
  let windowWidth = document.documentElement.clientWidth;
  let currentSlide = 1;

  window.addEventListener("resize", function (e) {
    windowWidth = document.documentElement.clientWidth;
    if (windowWidth < 1440) {
      countSlide = 4;
    }
    if (windowWidth < 1100) {
      countSlide = 3;
    }
  });
  if (windowWidth < 1440) {
    countSlide = 4;
  }
  if (windowWidth < 1100) {
    countSlide = 3;
  }

  sliders.forEach(function (item) {
    item.addEventListener("mouseover", function () {
      if (windowWidth > 980) {
        openSlide(item);
      }
    });
    item.addEventListener("mouseout", function () {
      if (windowWidth > 980) {
        closeSlide(item);
      }
    });
  });

  const swiper = new Swiper(".portfolio-section-slider", {
    slidesPerView: 5,
    // updateOnWindowResize: true,
    navigation: {
      nextEl: ".slider-next.portfolio-section-next",
      prevEl: ".slider-prev.portfolio-section-prev",
    },
    on: {
      slideChange: function () {
        currentSlide = swiper.activeIndex;
      },
    },
    breakpoints: {
      // when window width is >= 320px
      1440: {
        slidesPerView: 5,
        allowTouchMove: false,
      },
      1100: {
        slidesPerView: 4,
        allowTouchMove: false,
      },
      980: {
        slidesPerView: 3,
        allowTouchMove: false,
      },
      300: {
        allowTouchMove: true,
        slidesPerView: "auto",
      },
    },
  });

  function openSlide(activeItem) {
    windowWidth = document.documentElement.clientWidth;
    activeItem.classList.add("open-slide");
    let widthCalc;
    let box = activeItem.parentElement;
    let boxWidth = box.getBoundingClientRect().width;
    let activeItemWidth = 567;
    if (
      box.parentElement.parentElement.classList.contains("content-portfolio")
    ) {
      activeItemWidth = 436;
    }
    widthCalc = (boxWidth - activeItemWidth) / (countSlide - 1);

    sliders.forEach(function (item, index) {
      if (item !== activeItem && index >= currentSlide) {
        item.classList.add("short-slide");
        item.style.width = widthCalc + "px";
      }
    });
  }
  function closeSlide(activeItem) {
    windowWidth = document.documentElement.clientWidth;
    activeItem.classList.remove("open-slide");
    sliders.forEach(function (item) {
      if (item.classList.contains("short-slide")) {
        let widthCalc = 100 / countSlide;
        item.style.width = widthCalc + "%";
        item.classList.remove("short-slide");
      }
    });
  }

  function isHidden(el) {
    return el.offsetParent === null;
  }

  // Слайдеры на главной

  (function () {
    if (window.screen.width >= 769) return;
    $(".advantages-block").slick({
      // slidesToShow: 4,
      variableWidth: true,
      arrows: false,
    });
  })();

  $(".products-slider").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: $(".products-prev"),
    nextArrow: $(".products-next"),
    responsive: [
      {
        breakpoint: 993,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 769,
        settings: {
          variableWidth: true,
          arrows: false,
        },
      },
    ],
  });

  $(".partners-slider").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: $(".partners-prev"),
    nextArrow: $(".partners-next"),
    responsive: [
      {
        breakpoint: 993,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 769,
        settings: {
          variableWidth: true,
          arrows: false,
        },
      },
    ],
  });
  // $(".portfolio-section-slider").slick({
  //   // infinity: true,
  //   slidesToShow: 4,
  //   slidesToScroll: 1,
  //   variableWidth: true,
  //   prevArrow: $(".portfolio-section-prev"),
  //   nextArrow: $(".portfolio-section-next"),
  // });

  // Слайдеры на странице услуг
  // $(".content-projects__block-img").slick({
  //   slidesToShow: 5,
  //   slidesToScroll: 1,
  //   dots: false,
  //   arrows: false,
  //   focusOnSelect: true,
  //   asNavFor: ".content-projects__block-banner",
  // });
  // $(".content-projects__block-banner").slick({
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  //   dots: false,
  //   arrows: false,
  //   vertical: true,
  //   asNavFor: ".content-projects__block-img",
  // });
  const projectsSliderThumbs = new Swiper(".content-projects__block-img", {
    spaceBetween: 40,
    slidesPerView: "auto",
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    breakpoints: {
      769: {
        spaceBetween: 40,
      },
      320: {
        spaceBetween: 20,
      },
    },
  });
  const projectsSlider = new Swiper(".content-projects__block-banner", {
    slidesPerView: 1,
    effect: "fade",
    fadeEffect: {
      crossFade: true,
    },
    thumbs: {
      swiper: projectsSliderThumbs,
    },
  });
  const processSliderThumbs = new Swiper(".content-process__nav", {
    spaceBetween: 40,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    direction: "vertical",
    navigation: {
      nextEl: ".content-process__next",
      prevEl: ".content-process__prev",
    },
    breakpoints: {
      993: {
        slidesPerView: 4,
        direction: "vertical",
        spaceBetween: 40,
      },
      769: {
        slidesPerView: 1,
        direction: "horizontal",
        spaceBetween: 40,
      },
      320: {
        slidesPerView: 1,
        direction: "horizontal",
        spaceBetween: 20,
      },
    },
  });
  const processSlider = new Swiper(".content-process__for", {
    slidesPerView: 1,
    effect: "fade",
    fadeEffect: {
      crossFade: true,
    },
    thumbs: {
      swiper: processSliderThumbs,
    },
    navigation: {
      nextEl: ".content-process__next",
      prevEl: ".content-process__prev",
    },
  });
  // $(".content-process__nav").slick({
  //   slidesToShow: 4,
  //   slidesToScroll: 1,
  //   dots: false,
  //   vertical: true,
  //   focusOnSelect: true,
  //   prevArrow: $(".content-process__prev"),
  //   nextArrow: $(".content-process__next"),
  //   asNavFor: ".content-process__for",
  // });
  // $(".content-process__for").slick({
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  //   dots: false,
  //   arrows: false,
  //   vertical: true,
  //   asNavFor: ".content-process__nav",
  // });

  // Конетент-баннер
  $(".banner-page").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    prevArrow: $(".banner-page__button-prev"),
    nextArrow: $(".banner-page__button-next"),
  });

  // Страница услуг
  $(".content-service__block").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: $(".portfolio-blog__prev"),
    nextArrow: $(".portfolio-blog__next"),
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 993,
        settings: {
          slidesToShow: 1,
          variableWidth: true,
          arrows: false,
        },
      },
    ],
  });

  // Шаблонные слайдеры
  (function () {
    const portfolioCards = $(".portfolio-cards").find(".cards-block");
    const portfolioCardsList = $(".portfolio-list").find(".cards-block");
    const serviceSteps = $(".content-steps__block");
    const serviceAdvantages = $(".content-advantages").find(
      ".content-advantages-block"
    );

    createSlider(portfolioCards);
    createSlider(portfolioCardsList);
    createSlider(serviceSteps);
    createSlider(serviceAdvantages);
    
  })();

  // Страница портфолио
  $(".portfolio-blog__slider").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: $(".portfolio-blog__prev"),
    nextArrow: $(".portfolio-blog__next"),
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 993,
        settings: {
          variableWidth: true,
          arrows: false,
        },
      },
    ],
  });

  (function(){
    const wrapper = $(".portfolio-detail").find('.cards-block');
    const arrowNext = $(".portfolio-detail__slider-next");
    const arrowPrev = $(".portfolio-detail__slider-prev");

    slider(wrapper, arrowPrev, arrowNext)
  })();


  function slider (wrapper, arrowPrev, arrowNext){
    wrapper.slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: arrowNext,
      nextArrow: arrowPrev,
      responsive: [
        {
          breakpoint: 1201,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 993,
          settings: {
            variableWidth: true,
            arrows: false,
          },
        },
      ],
    });
  }

  // Страница о нас
  $(".about-comand__slider").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    focusOnSelect: true,
    centerMode: true,
    centerPadding: "12.5%",
    initialSlide: 1,
    prevArrow: $(".about-comand__prev"),
    nextArrow: $(".about-comand__next"),
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 993,
        settings: {
          centerMode: false,
          centerPadding: "0",
          variableWidth: true,
          arrows: false,
        },
      },
    ],
  });

  // Страница с отзывами
  $(".reviews-slider").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    prevArrow: $(".reviews-prev"),
    nextArrow: $(".reviews-next"),
  });

  // Страница статьи
  $(".articles-slider").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: $(".articles-prev"),
    nextArrow: $(".articles-next"),
    responsive: [
      {
        breakpoint: 993,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 769,
        settings: {
          variableWidth: true,
          arrows: false,
        },
      },
    ],
  });
});

// шаблон слайдера
function createSlider (slider) {

  if (window.screen.width >= 993) return;

  slider.slick({
    variableWidth: true,
    arrows: false,
    dots: false,
  });
};