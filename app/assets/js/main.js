$(document).ready(function () {
  // Открытие меню
  let headerButton = document.querySelector(".header-menu__button a");
  let headerLine = headerButton.querySelector("span");
  let topNav = $(".header-menu__block");
  const banner = $(".banner");
  const bannerTitle = banner.find("h1");
  const bannerMouse = banner.find(".banner-mouse");
  const bannerCategory = banner.find(".banner-block__item-category");
  const bannerItemBg = $(".banner-block__item-bg");
  const bannerSubmenu = $(".banner-block__item-submenu");

  headerButton.addEventListener("click", function (e) {
    e.preventDefault();
    if (!headerLine.classList.contains("active")) {
      headerLine.classList.add("active");
      topNav.slideDown();
    } else {
      headerLine.classList.remove("active");
      topNav.slideUp();
      bannerTitle.fadeIn();
      bannerMouse.fadeIn();
      bannerCategory.removeClass("active");
      bannerSubmenu.removeClass("active-menu");
    }
  });

  // Показ подменю
  (function () {
    const headerItem = $(".header-menu__mainline-item");
    const headerItemArrow = headerItem.children(".main-link").find("svg");

    if (window.screen.width >= 1024) {
      headerItem.hover(
        function () {
          showHeaderSubline(this);
        },
        function () {
          hideHeaderSubline(this);
        }
      );
    } else {
      headerItemArrow.on("click", function () {
        const arrow = $(this);
        const headerLink = $(this).siblings();
        const headerSubline = arrow
          .parent()
          .parent()
          .find(".header-menu__subline");

        if (headerSubline.length === 1) {
          headerSubline.slideToggle();
          toggleActive(arrow);
          toggleActive(headerLink);
        }
      });
    }

    function toggleActive(el) {
      if (el.hasClass("active")) {
        el.removeClass("active");
      } else {
        el.addClass("active");
      }
    }

    function showHeaderSubline(el) {
      const headerSubline = $(el).find(".header-menu__subline");
      if (headerSubline.length === 1) headerSubline.addClass("active");
    }

    function hideHeaderSubline(el) {
      const headerSubline = $(el).find(".header-menu__subline");
      if (headerSubline.length === 1) headerSubline.removeClass("active");
    }
  })();
  //

  // Выделение пункта меню на странице
  (function () {
    const locHref = window.location.pathname;
    const mainMenuItem = $(".main-link a");

    mainMenuItem.each(function () {
      let linkCheck = locHref.indexOf(`${$(this).attr("href")}`);

      if (!(linkCheck === -1)) $(this).addClass("active");
    });
  })();
  //

  // Работа баннера на главной
  (function () {
    bannerTitle.on("click", function () {
      if (window.screen.width <= 992) return;
      bannerTitle.fadeOut();
      bannerMouse.fadeOut();
      bannerCategory.addClass("active");
      headerLine.classList.add("active");
    });

    // bannerCategory.find("h2").on("click", function (e) {
    //   e.preventDefault();
    //   let submenu = $(this)
    //     .parents(".banner-block__item")
    //     .find(".banner-block__item-submenu");
    //   let categoryBg = $(this)
    //     .parents(".banner-block__item")
    //     .find(".banner-block__item-bg");
    //   let link = $(this).children("a").attr("href");

    //   if (submenu.length) {
    //     $(this).parents(".banner-block__item-category").removeClass("active");
    //     // bannerItemBg.css("filter", "blur(15)");
    //     submenu.addClass("active-menu");
    //   } else {
    //     location.href = link;
    //   }
    // });
  })();

  // Список технологий производства
  (function () {
    const listItems = $(".steps-list li");
    const listItemsText = $(".steps-list__text");
    const listItemsImg = listItemsText.find("img");

    listItems.each(function (index) {
      createNum($(this), index);
      if (!(window.screen.width <= 992))
        $(this).css("margin-left", `${index * 20}px`);
    });

    listItemsText.on({
      mouseenter: function () {
        listItemsImg.css("visibility", "visible");
        listItemsImg.css("opacity", "1");
      },
      mouseleave: function () {
        listItemsImg.css("visibility", "hidden");
        listItemsImg.css("opacity", "0");
      },
      mousemove: function (e) {
        listItemsImg.css({ top: e.pageY, left: e.pageX });
      },
    });

    function createNum(li, index) {
      let numTag = `<span class="steps-list__number num">0${index + 1}</span>`;
      li.prepend(numTag);
    }
  })();
  //

  // Ширина слайда
  (function () {
    const slider = $(".portfolio-section-slider");

    slider.on("beforeChange", function (event, slick, currentSlide) {
      const slide = $(this).find(".slick-slide");
      const active = $(this).find(".slick-active");
      const mainSlide = $(this).find(".slick-current");

      // slide.css("width", "20vw");
      // // active.css("width", "20vw");
      // mainSlide.css("width", `40vw`);
    });
  })();
  //

  // Блоки с вакансиями
  (function () {
    const btnInfo = $(".button-block");

    btnInfo.on("click", function () {
      const btn = $(this);
      const infoBlock = btn.parent().siblings();
      let time;

      infoBlock.slideToggle();
      btn.addClass("active");

      setTimeout(function () {
        btn.removeClass("active");
        infoBlock.slideToggle();
      }, 5000);

      // infoBlock.hover(
      //   function () {
      //     clearTimeout(time);
      //   },
      //   function () {
      //     showInfo(btn, infoBlock);
      //   }
      // );
    });

    // function showInfo(btn, infoBlock) {
    //   time = setTimeout(function () {
    //     btn.removeClass("active");
    //     infoBlock.slideToggle();
    //   }, 5000);
    // }
  })();

  // Скрытие меню на странице контетнта
  (function () {
    const titleMenu = $(".banner-content__header h2");
    const leftMenu = titleMenu
      .parent()
      .siblings()
      .find(".banner-content-block__item");
    const content = leftMenu.siblings().find(".content-block");

    titleMenu.on("click", function () {
      $(this).find("img").toggleClass("active");
      leftMenu.toggleClass("active");
      content.toggleClass("active");

      if (window.screen.width <= 992) $("body").toggleClass("active");
      if (!(window.screen.width <= 992)) {
        content.toggleClass("google");
        $(".banner-page").slick("slickNext");
      }

    });
  })();
});
