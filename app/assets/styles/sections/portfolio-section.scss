.portfolio-section {
  padding: 100px 0 0 0;

  &-slider {
    height: 582px;

    &__item {
      transition: .3s;

      & img {
        height: 582px;
        object-fit: cover;
        width: 100%;
      }
    }
  }

  & .slick {
    &-list.draggable {
      height: 582px;
    }

    &-active {
      position: relative;
      width: 20vw;
    }

    &-current {
      width: 40vw;
      transition: 1s;

      & img {
        width: inherit;
        object-fit: none;
      }
    }
  }
}

.portfolio-section-slider {
  width: 100%;

  .swiper-wrapper {
    background: rgba(39, 39, 39, 0.57);
  }

  &__item {
    width: calc(100%/5);
    position: relative;

    &_title {
      position: absolute;
      color: #fff;
      font-weight: bold;
      font-size: 48px;
      line-height: 55px;
      text-transform: uppercase;
      top: 17px;
      left: 24px;
      opacity: 0;
      transition: .3s;

      &.broken-title {
        left: 12%;
      }
    }

    &:before {
      content: '';
      position: absolute;
      width: 100%;
      height: 100%;
      background: rgba(39, 39, 39, 0.57);
      transition: opacity .3s ease;
    }
  }

  .swiper-slide-active {
    .portfolio-section-slider__item_title {}
  }

  &__item.open-slide {
    width: 576px !important;

    &:before {
      opacity: 0;
    }

    .portfolio-section-slider__item_title {
      opacity: 1;
    }

  }

  &__title {
    position: absolute;
    z-index: 1;
    right: 40px;
    bottom: 50px;

    h2 {
      font-weight: bold;
      font-size: 76.8px;
      line-height: 120%;
      text-transform: uppercase;
      color: #fff;
    }

    img {
      margin-left: 210px;
    }

  }

  .mobile-arrow {
    display: none;
  }

}

@media screen and (max-width: 1440px) {
  .portfolio-section-slider {
    &__item {
      width: calc(100% /4);
      position: relative;
    }
  }
}

@media screen and (max-width: 1100px) {
  .portfolio-section-slider {
    &__item {
      width: calc(100% /3);
      position: relative;
    }
  }
}

@media screen and (max-width: 1201px) {
  .portfolio-section {
    padding: 80px 0 0 0;
  }
}

@media screen and (max-width: 980px) {
  .portfolio-section-block__buttons {
    display: none;
  }

  .portfolio-section-slider {
    .swiper-wrapper {
      margin-left: 40px;
      padding-right: 40px !important;
      background: none;
    }

    &__item {
      width: 400px !important;
      height: 400px;
      position: relative;
      margin-right: 40px;

      &:before {
        display: none;
      }
    }

    img {
      height: auto;
    }

    .portfolio-section-slider__title {
      bottom: 27px;

      h2 {
        color: #272727;
        font-size: 46px;
      }

      img {
        margin-left: 116px;
      }
    }

    .desktop-arrow {
      display: none;
    }

    .mobile-arrow {
      display: block;
    }

  }
}

@media screen and (max-width: 768px) {
  .portfolio-section-slider {
    .portfolio-section-slider__title {
      bottom: 30px;

      h2 {
        color: #272727;
        font-size: 32px;
      }

      img {
        width: 220px;
        margin-left: 40px;
      }
    }
  }
}
