<div class="reviews-slider__item"><img src="[[+review_img]]" alt="[[+review_name]]">
    <div class="reviews-slider__item-block">
      <h3>[[+review_name]]</h3>
      <h4>[[+review_place]]</h4>
      <p>[[+review_text]]</p>
  </div>
</div>