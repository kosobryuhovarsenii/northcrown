<div class="about-comand__slider-item">
              <div class="about-comand__slider-item__img"><img src="[[+human_img]]" alt=""></div>
              <div class="about-comand__slider-item__text">
                <h3>[[+human_name]]</h3>
                <p>[[+human_place]]</p>
              </div>
            </div>