  <footer class="footer">
    <div class="container">
      <div class="footer-block">

        [[!pdoMenu?
        &parents = `0`
        &level = `1`
        &tplOuter = `@INLINE <ul class="footer-block__menu">[[+wrapper]]</ul>`
        &tpl = ` @INLINE <li><a href="/[[+link]]">[[+menutitle]]</a></li>`
        ]]

      </div>
      <div class="footer-block">
        <ul class="footer-block__info">
          <li><a href="tel:[[#7.number]]">[[#7.number]]</a></li>
          <li><a href="mailto:[[#7.email]]">[[#7.email]]</a></li>
          <li>
            <p>[[#7.address]]</p>
          </li>
        </ul>
        <ul class="footer-block__social">

          [[getImageList?
            &tvname=`social_table`
            &tpl=`tpl.social_item_footer`
            &docid=`7`
          ]]

        </ul>
      </div>
    </div>
  </footer>
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
  <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
  <script src="../../assets/js/app.min.js"></script>
  </div>
  </body>

  </html>