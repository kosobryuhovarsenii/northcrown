<section class="form">
  <div class="form-block">
    <div class="form-block__item">
      <h3 class="subtitle"> <span></span>[[+form_subtitle]]</h3>
      <h2 class="title">[[+form_title]]</h2>

      [[!AjaxForm?
        &snippet=`FormIt`
        &form=`tpl.form`
        &hooks=`email`
        &emailSubject=`Тестовое сообщение`
        &emailTo=`kosobryhovarsenii@gmail.com`
        &validate=`name:required,email:required,phone:required`
        &validationErrorMessage=`В форме содержатся ошибки!`
        &successMessage=`Сообщение успешно отправлено`
      ]]

    </div>
  </div>
  <div class="form-img" style="background:url('[[+form_img]]') center no-repeat; background-size: cover;">
  </div>
</section>