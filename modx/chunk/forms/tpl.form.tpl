<form action="" method="post" class="ajax_form form-item">

    <div class="controls">
        <input type="text" name="name" placeholder="Имя" value="[[+fi.name]]" class="form-control">
        <!-- <span class="error_name">[[+fi.error.name]]</span> -->
    </div>
    <div class="controls">
        <input type="tel" name="phone" placeholder="Телефон" value="[[+fi.phone]]" class="form-control">
        <!-- <span class="error_phone">[[+fi.error.phone]]</span> -->
    </div>
    <div class="controls">
        <input type="email" name="email" placeholder="E-mail" value="[[+fi.email]]" class="form-control">
        <!-- <span class="error_email">[[+fi.error.name]]</span> -->
    </div>
    <div class="controls">
        <button class="button" type="submit">Отправить</button>
    </div>

</form>