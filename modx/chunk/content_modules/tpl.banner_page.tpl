<section class="banner-page" style="background:url('../assets/img/portfolio/portfolio-banner.jpg') center no-repeat; background-size: cover;">
  <h2>
    <span> Оформляем пространство, внедряем <br>современный дизайн в городскую среду.</span>
    <a class="button" href="/">Оставить заявку</a>
  </h2>
</section>