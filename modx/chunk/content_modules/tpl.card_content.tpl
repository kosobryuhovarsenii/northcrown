<div class="cards-block__item">
  <div class="cards-block__item-img"><img src="[[+tv.portfolio-card_item-img]]" alt=""></div>
  <div class="cards-block__item-text">
    <h2>[[+pagetitle]]</h2>
    <p>[[+description]]</p>
    <a href="/">Смортеть<img src="../assets/img/service/service-arrow.svg" alt=""></a>
  </div>
</div>