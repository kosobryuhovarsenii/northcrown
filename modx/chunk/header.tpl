<!DOCTYPE html>
<html lang="ru-RU">

<head>
  <meta charset="utf-8">
  <meta class="viewport" name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="">
  <meta name="description" content="">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap" rel="stylesheet">
  <!-- Add the slick-theme.css if you want default styling -->
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
  <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css">
  <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
  <link rel="stylesheet" type="text/css" href="../../assets/css/main.min.css">
  <title>[[*pagetitle]]</title>
</head>

<body>
  <div class="wrapper" id="wrapper">
    <header class="header">
      <div class="container">
        <div class="header-block">
          <div class="header-logo"><a href="/"><img src="../../assets/img/header_logo.svg" alt=""></a></div>
          <div class="header-button"><a href="#">Обсудить проект</a></div>
          <div class="header-menu__button"><a href="#"> <span></span></a></div>
        </div>
        <div class="header-menu__block">
          
          [[!pdoMenu?
              &parents = `0`
              &level = `2`
              &rowClass=`header-menu__mainline-item`
              &tplOuter = `@INLINE <ul class="header-menu__mainline">[[+wrapper]]</ul>`
              &tpl = ` @INLINE 
                <li[[+classes]]>
                  <div class="main-link"><a href="/[[+link]]">[[+menutitle]]</a><svg width="26" height="26" viewBox="0 0 26 26"
                      fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M12.2148 24.7837L23.9986 12.9999L12.2148 1.21619" stroke="#A3A3A3" stroke-width="1.47" />
                    </svg></div>
                </li>
                `
              &tplInner=`@INLINE <ul class="header-menu__subline">[[+wrapper]]</ul>`
              &tplInnerRow=`@INLINE
                <li>
                  <a href="[[+link]]">[[+menutitle]]</a>
                  <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12.2148 24.7837L23.9986 12.9999L12.2148 1.21619" stroke="#A3A3A3" stroke-width="1.47" />
                  </svg>
                </li>`
              &tplParentRow=`@INLINE 
                <li[[+classes]]>
                  <div class="main-link">
                    <a href="/[[+link]]">[[+menutitle]]</a>
                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M12.2148 24.7837L23.9986 12.9999L12.2148 1.21619" stroke="#A3A3A3" stroke-width="1.47" />
                    </svg>
                  </div>
                  [[+wrapper]]
                </li>`
            ]]

        </div>
      </div>
    </header>