[[$header]]

<div class="content">
      <section class="banner">
        <h1 class="banner-title">Оформляем пространство, внедряем современный дизайн в городскую среду.</h1><a
          class="banner-mouse" href="/"><img src="../assets/img/banner-mouse.svg" alt=""></a>
        <div class="banner-block">
          <div class="banner-block-bg"></div>
          <div class="banner-block__item"
            style="background:url('./assets/img/banner_1.jpg') center no-repeat; background-size: cover;">
            <div class="banner-block__item-category">
              <h2> <a href="/">Блог</a></h2>
            </div>
            <div class="banner-block__item-submenu">
              <div class="banner-block__item-submenu__header">
                <h2>Городу</h2>
              </div>
              <ul class="banner-block__item-submenu__body">
                <li><a href="/">Городская мебель</a></li>
                <li><a href="/">Городская мебель</a></li>
                <li><a href="/">Городская мебель</a></li>
                <li><a href="/">Городская мебель</a></li>
                <li><a href="/">Городская мебель</a></li>
                <li><a href="/">Городская мебель</a></li>
                <li><a href="/">Городская мебель</a></li>
                <li><a href="/">Городская мебель</a></li>
                <li><a href="/">Городская мебель</a></li>
                <li><a href="/">Городская мебель </a></li>
              </ul>
            </div>
            <div class="banner-block__item-bg"></div>
          </div>
          <div class="banner-block__item"
            style="background:url('./assets/img/banner_2.jpg') center no-repeat; background-size: cover;">
            <div class="banner-block__item-category">
              <h2> <a href="https://api.jquery.com/children/">Блог</a></h2>
            </div>
            <div class="banner-block__item-bg"></div>
          </div>
          <div class="banner-block__item"
            style="background:url('./assets/img/banner_3.jpg') center no-repeat; background-size: cover;">
            <div class="banner-block__item-category">
              <h2> <a href="/">Блог</a></h2>
            </div>
            <div class="banner-block__item-bg"></div>
          </div>
          <div class="banner-block__item"
            style="background:url('./assets/img/banner_4.jpg') center no-repeat; background-size: cover;">
            <div class="banner-block__item-category">
              <h2> <a href="/">Блог</a></h2>
            </div>
            <div class="banner-block__item-bg"></div>
          </div>
          <div class="banner-block__item"
            style="background:url('./assets/img/banner_5.jpg') center no-repeat; background-size: cover;">
            <div class="banner-block__item-category">
              <h2> <a href="/">Блог</a></h2>
            </div>
            <div class="banner-block__item-bg"></div>
          </div>
        </div>
        <div class="banner-block__mobile"
          style="background:url('./assets/img/banner-mobile.jpg') center no-repeat; background-size: cover;"></div>
      </section>
      <section class="section catalog-section">
        <div class="container">
          <h3 class="subtitle"><span></span>Есть что сказать</h3>
          <h2 class="title">Мы делаем крутые объекты, оформляем рекламу и наполняем простанство вокруг вас удивительными
            вещами.</h2><a class="catalog-section__title" href="/">
            <h2>каталог</h2><img src="../assets/img/title-arrow.svg">
          </a>
        </div>
        <div class="catalog-section__banner"><img src="../assets/img/catalog-main.png" alt=""></div>
      </section>
      <section class="advantages">
        <div class="container">
          <div class="advantages-block">
            <div class="advantages-block__item">
              <div class="advantages-block__item-block"><img src="../assets/img/advant_0.svg" alt=""></div>
              <h3>Честность</h3>
            </div>
            <div class="advantages-block__item">
              <div class="advantages-block__item-block"><img src="../assets/img/advant_1.svg" alt=""></div>
              <h3>Экономия денег клиента</h3>
            </div>
            <div class="advantages-block__item">
              <div class="advantages-block__item-block"><img src="../assets/img/advant_2.svg" alt=""></div>
              <h3>Забота о каждом клиенте</h3>
            </div>
            <div class="advantages-block__item">
              <div class="advantages-block__item-block"><img src="../assets/img/advant_3.svg" alt=""></div>
              <h3>Уникальные и заметные композиции</h3>
            </div>
            <div class="advantages-block__item">
              <div class="advantages-block__item-block"><img src="../assets/img/advant_4.svg" alt=""></div>
              <h3>Экологичность</h3>
            </div>
          </div>
        </div>
      </section>
      <section class="products">
        <div class="container">
          <div class="title-block">
            <div class="title-block__text">
              <h3 class="subtitle"> <span></span>Это не конец</h3>
              <h2 class="title">Вот что мы еще можем</h2>
            </div>
            <div class="title-block__buttons">
              <div class="slider-prev products-prev">
                <p>назад</p><img src="../assets/img/slider-prev.svg" alt="">
              </div>
              <div class="slider-next products-next">
                <p>Вперёд</p><img src="../assets/img/slider-next.svg" alt="">
              </div>
            </div>
          </div>
          <div class="products-slider">
            <div class="products-slider__item">
              <div class="products-slider__item-info"><img src="../assets/img/product_1.jpg" alt="">
                <h4>от 12 000 ₽</h4>
                <p>Световые вывески</p>
              </div><a class="products-slider__item-button button" href="/">Заказать</a>
            </div>
            <div class="products-slider__item">
              <div class="products-slider__item-info"><img src="../assets/img/product_1.jpg" alt="">
                <h4>от 12 000 ₽</h4>
                <p>Световые вывески</p>
              </div><a class="products-slider__item-button button" href="/">Заказать</a>
            </div>
            <div class="products-slider__item">
              <div class="products-slider__item-info"><img src="../assets/img/product_1.jpg" alt="">
                <h4>от 12 000 ₽</h4>
                <p>Световые вывески</p>
              </div><a class="products-slider__item-button button" href="/">Заказать</a>
            </div>
            <div class="products-slider__item">
              <div class="products-slider__item-info"><img src="../assets/img/product_1.jpg" alt="">
                <h4>от 12 000 ₽</h4>
                <p>Световые вывески</p>
              </div><a class="products-slider__item-button button" href="/">Заказать</a>
            </div>
            <div class="products-slider__item">
              <div class="products-slider__item-info"><img src="../assets/img/product_1.jpg" alt="">
                <h4>от 12 000 ₽</h4>
                <p>Световые вывески</p>
              </div><a class="products-slider__item-button button" href="/">Заказать</a>
            </div>
          </div>
        </div>
      </section>
      <section class="portfolio-section">
        <div class="container">
          <div class="title-block">
            <div class="title-block__text">
              <h3 class="subtitle"> <span></span>говорит сама за себя</h3>
              <h2 class="title">Работа, за которую не стыдно</h2>
            </div>
            <div class="title-block__buttons">
              <div class="slider-prev portfolio-section-prev">
                <p>назад</p><img src="../assets/img/slider-prev.svg" alt="">
              </div>
              <div class="slider-next portfolio-section-next">
                <p>Вперёд</p><img src="../assets/img/slider-next.svg" alt="">
              </div>
            </div>
          </div>
        </div>
        <div class="portfolio-section-slider swiper-container">
          <div class="swiper-wrapper">
            <div class="portfolio-section-slider__item swiper-slide">
              <div class="portfolio-section-slider__item_title">Фигуры</div><img src="../assets/img/portfolio_1.jpg"
                alt="">
            </div>
            <div class="portfolio-section-slider__item swiper-slide">
              <div class="portfolio-section-slider__item_title">Фигуры</div><img src="../assets/img/portfolio_1.jpg"
                alt="">
            </div>
            <div class="portfolio-section-slider__item swiper-slide">
              <div class="portfolio-section-slider__item_title">Фигуры</div><img src="../assets/img/portfolio_1.jpg"
                alt="">
            </div>
            <div class="portfolio-section-slider__item swiper-slide">
              <div class="portfolio-section-slider__item_title">Фигуры</div><img src="../assets/img/portfolio_1.jpg"
                alt="">
            </div>
            <div class="portfolio-section-slider__item swiper-slide">
              <div class="portfolio-section-slider__item_title">Фигуры</div><img src="../assets/img/portfolio_1.jpg"
                alt="">
            </div>
            <div class="portfolio-section-slider__item swiper-slide">
              <div class="portfolio-section-slider__item_title">Фигуры</div><img src="../assets/img/portfolio_1.jpg"
                alt="">
            </div>
            <div class="portfolio-section-slider__item swiper-slide">
              <div class="portfolio-section-slider__item_title">Фигуры</div><img src="../assets/img/portfolio_1.jpg"
                alt="">
            </div>
          </div>
          <div class="portfolio-section-slider__title" href="/">
            <h2>Портфолио</h2><img class="desktop-arrow" src="../assets/img/title-arrow-white.svg" alt=""><img
              class="mobile-arrow" src="../assets/img/title-arrow.svg" alt="">
          </div>
        </div>
      </section>
      <section class="steps">
        <div class="container">
          <h3 class="subtitle"> <span></span>ответственный подход</h3>
          <h2 class="title">Технологии производства</h2>
          <ul class="steps-list">
            <li><span class="steps-list__text"> <img src="../assets/img/steps_1.jpg" alt="">Программируем световую
                иллюминацию. Освещаем фасады</span></li>
            <li><span class="steps-list__text"> <img src="../assets/img/steps_1.jpg" alt="">Варим нержавеющий металл и
                алюминий.</span></li>
            <li><span class="steps-list__text"> <img src="../assets/img/steps_1.jpg" alt="">Формуем пластик с разными
                видами последующей обработки</span></li>
            <li><span class="steps-list__text"> <img src="../assets/img/steps_1.jpg" alt="">Работаем с деревом,
                выполняем качественную обработку, покраску.</span></li>
            <li><span class="steps-list__text"> <img src="../assets/img/steps_1.jpg" alt="">Пенопласт. Вырезаем фигуры и
                наносим стойкое покрытие.</span></li>
            <li><span class="steps-list__text"> <img src="../assets/img/steps_1.jpg" alt="">Работаем с гибкими
                источниками света.</span></li>
            <li><span class="steps-list__text"> <img src="../assets/img/steps_1.jpg" alt="">Экструдирование прозрачного
                полистирола. Позволяет нам имитировать лед.</span></li>
            <li><span class="steps-list__text"> <img src="../assets/img/steps_1.jpg" alt="">Работаем со стеклопластиком
                для создания объемных изделий</span></li>
            <li><span class="steps-list__text"> <img src="../assets/img/steps_1.jpg" alt="">Полноцветная печать на любые
                поверхности: пластик, баннер, стекло, алюминий</span></li>
          </ul>
        </div>
      </section>
      <section class="form">
        <div class="form-block">
          <div class="form-block__item">
            <h3 class="subtitle"> <span></span>мы можем помочь</h3>
            <h2 class="title">Форма обратной связи</h2>
            <form class="form-item" action="" method="post">
              <div><input type="text" name="name" placeholder="Имя"></div>
              <div><input type="tel" name="phone" placeholder="Телефон"></div>
              <div><input type="email" name="email" placeholder="E-mail"></div>
              <div><button class="button" type="submit">Отправить</button></div>
            </form>
          </div>
        </div>
        <div class="form-img"
          style="background:url('../assets/img/banner_1.jpg') center no-repeat; background-size: cover;"></div>
      </section>
      <section class="partners">
        <div class="container">
          <h3 class="subtitle"> <span></span>они нам доверяют</h3>
          <h2 class="title">НАши партнёры</h2>
          <div class="partners-block">
            <div class="slider-prev partners-prev">
              <p>назад</p><img src="../assets/img/slider-prev.svg" alt="">
            </div>
            <div class="partners-slider">
              <div class="partners-slider__item">
                <div><img src="../assets/img/partners_1.png" alt=""></div>
              </div>
              <div class="partners-slider__item">
                <div><img src="../assets/img/partners_2.png" alt=""></div>
              </div>
              <div class="partners-slider__item">
                <div><img src="../assets/img/partners_3.png" alt=""></div>
              </div>
              <div class="partners-slider__item">
                <div><img src="../assets/img/partners_4.png" alt=""></div>
              </div>
              <div class="partners-slider__item">
                <div><img src="../assets/img/partners_4.png" alt=""></div>
              </div>
            </div>
            <div class="slider-next partners-next">
              <p>Вперёд</p><img src="../assets/img/slider-next.svg" alt="">
            </div>
          </div>
        </div>
      </section>
      <section class="social-section">
        <div class="container">
          <h3 class="subtitle"> <span></span>следим за трендами</h3>
          <h2 class="title">Социальные сети</h2>
          <div class="social-section-block">
            <div class="social-section-block__item"><a href="/"><img src="../assets/img/social-vk.svg" alt=""></a></div>
            <div class="social-section-block__item"><a href="/"><img src="../assets/img/social-inst.svg" alt=""></a>
            </div>
            <div class="social-section-block__item"><a href="/"><img src="../assets/img/social-facebook.svg" alt=""></a>
            </div>
            <div class="social-section-block__item"><a href="/"><img src="../assets/img/social-zen.svg" alt=""></a>
            </div>
            <div class="social-section-block__item"><a href="/"><img src="../assets/img/social-be.svg" alt=""></a></div>
          </div>
        </div>
      </section>
    </div>

[[$footer]]