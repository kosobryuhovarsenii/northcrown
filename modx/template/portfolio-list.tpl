[[$header]]

<div class="content">
  <section class="banner-content">
    <div class="banner-content__header">
      <h2> <img class="header-left" src="../../assets/img/service/service-arrow-prev.svg" alt="">[[*pagetitle]]<img
          class="header-right" src="../../assets/img/service/service-arrow.svg" alt=""></h2>
    </div>
    <div class="banner-content-block">
      <div class="banner-content-block__item"
        style="background:url('../../assets/img/banner_1.jpg') center no-repeat; background-size: cover;">
        <div class="banner-content-block__item-submenu active-menu">
          <ul class="banner-content-block__item-submenu__body">
            [[!msProducts?
              &parents=`[[*id]]`
              &tpl=`@INLINE <li><a href="/[[+uri]]">[[+pagetitle]]</a></li>`
              &limit=`0`
            ]]
          </ul>
        </div>
        <div class="banner-content-block__item-bg"></div>
      </div>
      <div class="content">
        <div class="content-block google">
          <section class="portfolio-list">
            <h3 class="subtitle"> <span></span>[[*longtitle]]</h3>
            <h2 class="title">[[*pagetitle]]</h2>

            <div id="pdopage">
              <div class="cards-block pdo">

                [[!pdoPage?
                &parents=`[[*id]]`
                &element=`msProducts`
                &ajaxMode=`button`
                &limit=`4`
                &depth=`0`
                &sortby=`{"publishedon":"ASC"}`
                &tpl=`tpl.card-content`
                &ajaxElemRows=`.pdo`
                &ajaxTplMore=`@INLINE <button class="btn-more button">Показать еще</button>`
                ]]

              </div>

              [[!+page.nav]]
            </div>

            <div class="cards-block mobile">

              [[!msProducts?
              &parents=`[[*id]]`
              &limit=`6`
              &sortby=`{"publishedon":"DESC"}`
              &depth=`0`
              &tpl=`tpl.card-content`
              ]]

            </div>

            [[$tpl.section-form]]

          </section>
        </div>
      </div>
    </div>
  </section>
</div>

[[$footer]]