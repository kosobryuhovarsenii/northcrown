[[$header]]

    <div class="content">
      <div class="content-wrapper reviews-page">
        <section class="reviews-section">
          <div class="container">
            <h3 class="subtitle"><span></span>[[*longtitle]]</h3>
            <h2 class="title">[[*pagetitle]]</h2>
            <p class="reviews-description">[[*review_desc]]</p>
            
            
            <div class="reviews-slider">
              <div class="reviews-slider__slide rows">

                [[!pdoPage?
                  &parents=`0`
                  &pageLimit=`5`
                  &element=`getImageList`
                  &limit=`4`
                  &tvname=`review_table`
                  &tpl=`tpl.review_item`
                  &tplPagePrev=`review_prev`
                  &tplPageNext=`review_next`
                  &tplPageFirst=`@INLINE`
                  &tplPageLast=`@INLINE`
                  &tplPagePrevEmpty=`review_prev_empty`
                  &tplPageNextEmpty=`review_next_empty`
                  &tplPageFirstEmpty=`@INLINE`
                  &tplPageLastEmpty=`@INLINE`
                  &docid=`[[*id]]`
                  &ajaxMode=`default`
                ]]

                [[!+page.nav]]
              </div>
            </div>
          </div>
        </section>
        
        [[$tpl.setion-form]]

      </div>
    </div>

[[$footer]]