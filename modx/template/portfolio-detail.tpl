[[$header]]

<div class="content">
			<section class="banner-content">
				<div class="banner-content__header">
					<h2> <img class="header-left" src="../../../assets/img/service/service-arrow-prev.svg" alt="">[[*pagetitle]]<img
							class="header-right" src="../../../assets/img/service/service-arrow.svg" alt=""></h2>
				</div>
				<div class="banner-content-block">
					<div class="banner-content-block__item"
						style="background:url('../../../assets/img/banner_1.jpg') center no-repeat; background-size: cover;">
						<div class="banner-content-block__item-submenu active-menu">
							<ul class="banner-content-block__item-submenu__body">
								[[!msProducts?
                  &parents=`msProduct.parent`
                  &tpl=`@INLINE <li><a href="/[[+uri]]">[[+pagetitle]]</a></li>`
                  &limit=`0`
                ]]
							</ul>
						</div>
						<div class="banner-content-block__item-bg"></div>
					</div>
					<div class="content">
						<div class="content-block google">
							<section class="portfolio-detail">
								<div class="portfolio-detail__banner"
									style="background:url('/[[*portfolio-detail_img]]') center no-repeat; background-size: cover;">
									<h2>[[*longtitle]]</h2>
								</div>
								<div class="portfolio-detail__text">[[*content]]</div>
								<div class="content-projects__block">
									<div class="content-projects__block-banner swiper-container">
										<div class="swiper-wrapper">
											<div class="content-projects__block-banner__item swiper-slide"><img
													src="../../assets/img/service/project-main.jpg" alt=""></div>
											<div class="content-projects__block-banner__item swiper-slide"><img src="../../assets/img/map.jpg"
													alt=""></div>
											<div class="content-projects__block-banner__item swiper-slide"><img
													src="../../assets/img/service/project-main.jpg" alt=""></div>
											<div class="content-projects__block-banner__item swiper-slide"><img
													src="../../assets/img/service/project-main.jpg" alt=""></div>
											<div class="content-projects__block-banner__item swiper-slide"><img
													src="../../assets/img/service/project-main.jpg" alt=""></div>
										</div>
									</div>
									<div class="content-projects__block-img swiper-container">
										<div class="swiper-wrapper">
											<div class="content-projects__block-img__item swiper-slide"><img
													src="../../assets/img/service/project_1.jpg" alt=""></div>
											<div class="content-projects__block-img__item swiper-slide"><img
													src="../../assets/img/service/project_2.jpg" alt=""></div>
											<div class="content-projects__block-img__item swiper-slide"><img
													src="../../assets/img/service/project_3.jpg" alt=""></div>
											<div class="content-projects__block-img__item swiper-slide"><img
													src="../../assets/img/service/project_4.jpg" alt=""></div>
											<div class="content-projects__block-img__item swiper-slide"><img
													src="../../assets/img/service/project_5.jpg" alt=""></div>
										</div>
									</div>
								</div>
								<div class="portfolio-detail__price">
									<p> Похожий проект обойдется <span class="red">от <span class="price">100000 ₽</span></span>
									</p>
								</div>
								
                [[$tpl.section-form]]

								<section class="portfolio-detail__slider">
									<div class="title-block">
										<div class="title-block__text">
											<h3 class="subtitle"> <span></span>абсолютно индивидуальные</h3>
											<h2 class="title">Последние статьи в блоге</h2>
										</div>
										<div class="title-block__buttons">
											<div class="slider-prev portfolio-detail__slider-prev">
												<p>назад</p><img src="../../assets/img/slider-prev.svg" alt="">
											</div>
											<div class="slider-next portfolio-detail__slider-next">
												<p>Вперёд</p><img src="../../assets/img/slider-next.svg" alt="">
											</div>
										</div>
									</div>
									<div class="cards-block">
										<div class="cards-block__item">
											<div class="cards-block__item-img"><img src="../../assets/img/portfolio/portfolio-card_1.jpg"
													alt=""></div>
											<div class="cards-block__item-text">
												<h2>Оформление здания на проспекте Мира</h2>
												<p>Сделали индивидульный дизайн для города</p><a href="/">Смортеть<img
														src="../../assets/img/service/service-arrow.svg" alt=""></a>
											</div>
										</div>
										<div class="cards-block__item">
											<div class="cards-block__item-img"><img src="../../assets/img/portfolio/portfolio-card_2.jpg"
													alt=""></div>
											<div class="cards-block__item-text">
												<h2>Оформление здания на проспекте Мира</h2>
												<p>Сделали индивидульный дизайн для города</p><a href="/">Смортеть<img
														src="../../assets/img/service/service-arrow.svg" alt=""></a>
											</div>
										</div>
										<div class="cards-block__item">
											<div class="cards-block__item-img"><img src="../../assets/img/portfolio/portfolio-card_3.jpg"
													alt=""></div>
											<div class="cards-block__item-text">
												<h2>Оформление здания на проспекте Мира</h2>
												<p>Сделали индивидульный дизайн для города</p><a href="/">Смортеть<img
														src="../../assets/img/service/service-arrow.svg" alt=""></a>
											</div>
										</div>
										<div class="cards-block__item">
											<div class="cards-block__item-img"><img src="../../assets/img/portfolio/portfolio-card_4.jpg"
													alt=""></div>
											<div class="cards-block__item-text">
												<h2>Оформление здания на проспекте Мира</h2>
												<p>Сделали индивидульный дизайн для города</p><a href="/">Смортеть<img
														src="../../assets/img/service/service-arrow.svg" alt=""></a>
											</div>
										</div>
										<div class="cards-block__item">
											<div class="cards-block__item-img"><img src="../../assets/img/portfolio/portfolio-card_5.jpg"
													alt=""></div>
											<div class="cards-block__item-text">
												<h2>Оформление здания на проспекте Мира</h2>
												<p>Сделали индивидульный дизайн для города</p><a href="/">Смортеть<img
														src="../../assets/img/service/service-arrow.svg" alt=""></a>
											</div>
										</div>
										<div class="cards-block__item">
											<div class="cards-block__item-img"><img src="../../assets/img/portfolio/portfolio-card_6.jpg"
													alt=""></div>
											<div class="cards-block__item-text">
												<h2>Оформление здания на проспекте Мира</h2>
												<p>Сделали индивидульный дизайн для города</p><a href="/">Смортеть<img
														src="../../assets/img/service/service-arrow.svg" alt=""></a>
											</div>
										</div>
										<div class="cards-block__item">
											<div class="cards-block__item-img"><img src="../../assets/img/portfolio/portfolio-card_7.jpg"
													alt=""></div>
											<div class="cards-block__item-text">
												<h2>Оформление здания на проспекте Мира</h2>
												<p>Сделали индивидульный дизайн для города</p><a href="/">Смортеть<img
														src="../../assets/img/service/service-arrow.svg" alt=""></a>
											</div>
										</div>
										<div class="cards-block__item">
											<div class="cards-block__item-img"><img src="../../assets/img/portfolio/portfolio-card_8.jpg"
													alt=""></div>
											<div class="cards-block__item-text">
												<h2>Оформление здания на проспекте Мира</h2>
												<p>Сделали индивидульный дизайн для города</p><a href="/">Смортеть<img
														src="../../assets/img/service/service-arrow.svg" alt=""></a>
											</div>
										</div>
									</div>
								</section>
							</section>
						</div>
					</div>
				</div>
			</section>
		</div>

[[$footer]]