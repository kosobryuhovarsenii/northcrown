[[$header]]

<div class="content">
      <div class="content-wrapper contacts-page">
        <div class="contacts-info">
          <div class="container">
            <h3 class="subtitle"> <span></span>[[*longtitle]]</h3>
            <h2 class="title">[[*pagetitle]]</h2>
            <section class="contacts-block">
              <div class="contacts-block__address">
                <h4>Главный офис:</h4>
                <p>[[*address]]<br>Прием заявок: ПН-ПТ с 6.00 до 22.00 мск.</p>
              </div>
              <div class="contacts-block__info">
                <div class="contacts-block__info-block"><a href="tel:[[*number]]">[[*number]]</a><a
                    href="mailto:[[*email]]">[[*email]]</a></div>
                <ul class="contacts-block__info-social">

                  [[getImageList?
                    &tvname=`social_table`
                    &tpl=`tpl.social_item`
                  ]]

                </ul>
              </div>
            </section><img src="../assets/img/map.jpg" alt="" style="width:100%">
            <div id="contact-map"></div>
          </div>

          [[$tpl.section-form]]

        </div>
      </div>
    </div>

[[$footer]]