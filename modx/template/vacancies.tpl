[[$header]]

<div class="content">
      <div class="content-wrapper vacancies-page">
        <section class="vacancies-section">
          <div class="container">
            <h3 class="subtitle"><span></span>[[*longtitle]]</h3>
            <h2 class="title">[[*pagetitle]]</h2>
            <div class="vacancies-block">
              
              [[getImageList?
                    &tvname=`vacancy_table`
                    &tpl=`tpl.vacancy_item`
              ]]

            </div>
          </div>
        </section>

        [[$tpl.section-form]]

      </div>
    </div>

[[$footer]]