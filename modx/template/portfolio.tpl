[[$header]]

<div class="content">
      <div class="content-wrapper">
        <div class="container">
          
          [[$tpl.banner_page]]

          <section class="portfolio-zone">
            <h3 class="subtitle"> <span></span>для разных клиентов</h3>
            <h2 class="title">Разделы портфолио</h2>
            <div class="portfolio-zone__block">
              <div class="portfolio-zone__block-item"
                style="background: url('../assets/img/portfolio/portfolio-zone_1.jpg') center no-repeat; background-size: cover;">
                <a href="">арт-объекты</a></div>
              <div class="portfolio-zone__block-item"
                style="background: url('../assets/img/portfolio/portfolio-zone_1.jpg') center no-repeat; background-size: cover;">
                <a href="">городская среда</a></div>
              <div class="portfolio-zone__block-item"
                style="background: url('../assets/img/portfolio/portfolio-zone_2.jpg') center no-repeat; background-size: cover;">
                <a href="">рекламное</a></div>
              <div class="portfolio-zone__block-item"
                style="background: url('../assets/img/portfolio/portfolio-zone_3.jpg') center no-repeat; background-size: cover;">
                <a href="">фасады</a></div>
              <div class="portfolio-zone__block-item"
                style="background: url('../assets/img/portfolio/portfolio-zone_4.jpg') center no-repeat; background-size: cover;">
                <a href="">интерьеры</a></div>
            </div>
          </section>
          <section class="portfolio-cards">
            <h3 class="subtitle"> <span></span>говорит сама за себя</h3>
            <h2 class="title">Работа, за которую не стыдно</h2>
            <div class="cards-block">

              [[!pdoResources?
                  &parents=`5`
                  &depth=`0`
                  &tpl=`tpl.card-content`
                  &includeTVs=`portfolio-card_item-img`
              ]]

            </div>
          </section>
        </div>

        [[$tpl.section-form]]

        <div class="container">
          <section class="portfolio-blog">
            <div class="title-block">
              <div class="title-block__text">
                <h3 class="subtitle"> <span></span>абсолютно индивидуальные</h3>
                <h2 class="title">Последние статьи в блоге</h2>
              </div>
              <div class="title-block__buttons">
                <div class="slider-prev portfolio-blog__prev">
                  <p>назад</p><img src="../assets/img/slider-prev.svg" alt="">
                </div>
                <div class="slider-next portfolio-blog__next">
                  <p>Вперёд</p><img src="../assets/img/slider-next.svg" alt="">
                </div>
              </div>
            </div>
            <div class="portfolio-blog__slider">
              <div class="blog-item">
                <div class="blog-item__img"><img src="../assets/img/service/service_1.jpg" alt=""></div>
                <div class="blog-item__text">
                  <h3>Работа, которую нельзя обойти стороной. Тц...</h3><a href="/">Читать дальше<img
                      src="../assets/img/service/service-arrow.svg" alt=""></a>
                </div>
              </div>
              <div class="blog-item">
                <div class="blog-item__img"><img src="../assets/img/service/service_1.jpg" alt=""></div>
                <div class="blog-item__text">
                  <h3>Работа, которую нельзя обойти стороной. Тц...</h3><a href="/">Читать дальше<img
                      src="../assets/img/service/service-arrow.svg" alt=""></a>
                </div>
              </div>
              <div class="blog-item">
                <div class="blog-item__img"><img src="../assets/img/service/service_1.jpg" alt=""></div>
                <div class="blog-item__text">
                  <h3>Работа, которую нельзя обойти стороной. Тц...</h3><a href="/">Читать дальше<img
                      src="../assets/img/service/service-arrow.svg" alt=""></a>
                </div>
              </div>
              <div class="blog-item">
                <div class="blog-item__img"><img src="../assets/img/service/service_1.jpg" alt=""></div>
                <div class="blog-item__text">
                  <h3>Работа, которую нельзя обойти стороной. Тц...</h3><a href="/">Читать дальше<img
                      src="../assets/img/service/service-arrow.svg" alt=""></a>
                </div>
              </div>
              <div class="blog-item">
                <div class="blog-item__img"><img src="../assets/img/service/service_1.jpg" alt=""></div>
                <div class="blog-item__text">
                  <h3>Работа, которую нельзя обойти стороной. Тц...</h3><a href="/">Читать дальше<img
                      src="../assets/img/service/service-arrow.svg" alt=""></a>
                </div>
              </div>
              <div class="blog-item">
                <div class="blog-item__img"><img src="../assets/img/service/service_1.jpg" alt=""></div>
                <div class="blog-item__text">
                  <h3>Работа, которую нельзя обойти стороной. Тц...</h3><a href="/">Читать дальше<img
                      src="../assets/img/service/service-arrow.svg" alt=""></a>
                </div>
              </div>
              <div class="blog-item">
                <div class="blog-item__img"><img src="../assets/img/service/service_1.jpg" alt=""></div>
                <div class="blog-item__text">
                  <h3>Работа, которую нельзя обойти стороной. Тц...</h3><a href="/">Читать дальше<img
                      src="../assets/img/service/service-arrow.svg" alt=""></a>
                </div>
              </div>
              <div class="blog-item">
                <div class="blog-item__img"><img src="../assets/img/service/service_1.jpg" alt=""></div>
                <div class="blog-item__text">
                  <h3>Работа, которую нельзя обойти стороной. Тц...</h3><a href="/">Читать дальше<img
                      src="../assets/img/service/service-arrow.svg" alt=""></a>
                </div>
              </div>
              <div class="blog-item">
                <div class="blog-item__img"><img src="../assets/img/service/service_1.jpg" alt=""></div>
                <div class="blog-item__text">
                  <h3>Работа, которую нельзя обойти стороной. Тц...</h3><a href="/">Читать дальше<img
                      src="../assets/img/service/service-arrow.svg" alt=""></a>
                </div>
              </div>
              <div class="blog-item">
                <div class="blog-item__img"><img src="../assets/img/service/service_1.jpg" alt=""></div>
                <div class="blog-item__text">
                  <h3>Работа, которую нельзя обойти стороной. Тц...</h3><a href="/">Читать дальше<img
                      src="../assets/img/service/service-arrow.svg" alt=""></a>
                </div>
              </div>
              <div class="blog-item">
                <div class="blog-item__img"><img src="../assets/img/service/service_1.jpg" alt=""></div>
                <div class="blog-item__text">
                  <h3>Работа, которую нельзя обойти стороной. Тц...</h3><a href="/">Читать дальше<img
                      src="../assets/img/service/service-arrow.svg" alt=""></a>
                </div>
              </div>
              <div class="blog-item">
                <div class="blog-item__img"><img src="../assets/img/service/service_1.jpg" alt=""></div>
                <div class="blog-item__text">
                  <h3>Работа, которую нельзя обойти стороной. Тц...</h3><a href="/">Читать дальше<img
                      src="../assets/img/service/service-arrow.svg" alt=""></a>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>

[[$footer]]