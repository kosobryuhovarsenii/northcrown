[[$header]]

<div class="content">
      <div class="content-wrapper articles-page">
        <section class="articles-section">
          <div class="container">
            <div class="title-block">
              <div class="title-block__text">
                <h3 class="subtitle"> <span></span>[[*longtitle]]</h3>
                <h2 class="title">[[*pagetitle]]</h2>
              </div>
              <div class="title-block__buttons">
                <div class="slider-prev articles-prev">
                  <p>назад</p><img src="../assets/img/slider-prev.svg" alt="">
                </div>
                <div class="slider-next articles-next">
                  <p>Вперёд</p><img src="../assets/img/slider-next.svg" alt="">
                </div>
              </div>
            </div>
            <div class="articles-slider">

              [[getImageList?
                &tvname=`article_table`
                &tpl=`tpl.article_item`
              ]]

            </div>
          </div>
        </section>
      </div>
    </div>


[[$footer]]

