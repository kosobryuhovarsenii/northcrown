[[$header]]

<div class="content">
      <div class="content-wrapper about-page">
        <section class="about-info">
          <div class="container">
            <h3 class="subtitle"> <span></span>[[*longtitle]]</h3>
            <h2 class="title">[[*pagetitle]]</h2>
            <div class="about-info__block">

              [[*content]]

            </div>
          </div>
        </section>
        <section class="section about-comand">
          <div class="container">
            <h3 class="subtitle"> <span></span>[[*command_subtitle]]</h3>
            <h2 class="title">[[*command_title]]</h2>
            <div class="slider-prev about-comand__prev">
              <p>назад</p><img src="../assets/img/slider-prev.svg" alt="">
            </div>
            <div class="slider-next about-comand__next">
              <p>Вперёд</p><img src="../assets/img/slider-next.svg" alt="">
            </div>
          </div>
          <div class="about-comand__slider">

            [[getImageList?
              &tvname=`command_table`
              &tpl=`tpl.command_item`
            ]]

          </div>
        </section>

        [[$tpl.section-form]]

      </div>
    </div>

[[$footer]]